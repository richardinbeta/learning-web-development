* framework vs library vs patterns
* div vs span
* html
  * section
  * article
  * footer
* shadow dom
* custom elements
* web components
* ryb color model
* inline vs block elements
* document flow
* pseudo class vs pseudo elements
* margin vs padding
